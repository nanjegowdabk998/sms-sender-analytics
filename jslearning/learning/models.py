from __future__ import unicode_literals

from django.conf import settings

from django.core.files.storage import FileSystemStorage

from django.db import models

# Create your models here.
from rest_framework import serializers

def upload_location(instance, filename):
    return "%s/%s" % (instance.id, filename)


class Backup(models.Model):
    mobile = models.CharField(max_length=20)
    backup_file = models.FileField('backup_file', upload_to=upload_location, storage=FileSystemStorage(location=settings.MEDIA_ROOT), null=True, blank=True)


class BackupFileserializer(serializers.ModelSerializer):
    class Meta:
        model = Backup
        fields = ['backup_file']
