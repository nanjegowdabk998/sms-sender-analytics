angular.module('learning').controller('FirstCtrl', function($uibModal, first, $http, $scope) {

    $scope.sendmobile = function(data) {
        console.log(data)
        var mobile = "mobile_number=" + data
        var obj = new XMLHttpRequest();
        obj.open("POST", "http://127.0.0.1:8000/mobiledataupload");
        obj.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        obj.onload = function() {
            $scope.result = obj.responseText;
            console.log($scope.result);
        }
        obj.send(mobile);
    }

    $scope.visible = true;
    $scope.visible1 = false;
    $scope.visible2 = false;

    $scope.enablediabl = function() {
        $scope.visible = false;
        $scope.visible1 = true;
        $scope.visible2 = false;
    }
    $scope.makedisable = function() {
        $scope.visible = false;
        $scope.visible1 = false;
        $scope.visible2 = true;
    }
    $scope.uploadxmlfile = function(file, mobile) {
        console.log("my fiel is ", file);
        $scope.name = "backup_file";
        var fd = new FormData();
        fd.append('file', file);
        fd.append('name', $scope.name);
        fd.append('mobil', mobile);
        console.log("fddddddd", fd);
        return $http.post('http://127.0.0.1:8000/pic_upload', fd, {
                transformRequest: angular.identity,
                headers: { 'Content-Type': undefined }
            })
            .success(function(data) {
                console.log("iam success msg", data);
                $scope.result = data;
                var modalInstance = $uibModal.open({
                    templateUrl: "saveresponse",
                    controller: "PopupCtrl",
                    size: "md",
                    backdrop: "static",
                    keyboard: true,
                    scope: $scope
                });
            })
            .error(function(data) {
                $scope.result = data;
                var modalInstance = $uibModal.open({
                    templateUrl: "saveresponse",
                    controller: "PopupCtrl",
                    size: "md",
                    backdrop: "static",
                    keyboard: true,
                    scope: $scope
                });
            });
    }

    $scope.getcontent = function(mobile) {
        console.log("mobileee", mobile)
        return $http.post('/getjsoncontent', mobile).then(function(data) {
            console.log("content from backend", data.data);
            $scope.dashboarddata = data.data;
        })
    }

});

angular.module('learning').controller('PopupCtrl', function($scope, $uibModalInstance) {
    $scope.cancelpopup = function() {
        $uibModalInstance.close();
    }

});

angular.module('learning').directive('fileModel', function($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs, events) {

            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            element.bind('change', function() {
                scope.$apply(function() {
                    modelSetter(scope, element[0].files[0]);
                });

            });
        }
    }
});
