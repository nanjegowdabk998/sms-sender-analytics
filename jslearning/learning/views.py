from django.shortcuts import render

# Create your views here.
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from learning.models import *


@api_view(['POST'])
def mobiledataupload(request):
    if request.data['mobile_number'] not in ['undefined', '', 'null']:
        var = Backup()
        var.mobile = request.data['mobile_number']
        var.save()
        data = {"message":"Mobile Saved successfully", "status":200}
    else:
        data = {"message":"Mobile Number not Available", "status":400}
    return Response(data)


@api_view(['POST'])
def pic_upload(request):
    profile = Backup.objects.filter(mobile=request.data['mobil'])
    if 'file' in request.data:
        if str(request.data['file']).endswith('.json'):
            name = request.data['name']
            request.data[name] = request.data['file']
            adminserializer = BackupFileserializer(profile[0], request.data)
            if adminserializer.is_valid(Exception):
                adminserializer.save()
                data = {'message': 'file uploaded Successfully'}
        else:
            data = {'message': 'file type not allowed'}
    else:
        data = {'message': 'file type not allowed'}
    return Response(data, status=status.HTTP_200_OK)

import json
import re
@api_view(['POST'])
def readbackupfile(request):
    mobile = request.data
    details = Backup.objects.filter(mobile=mobile)
    list2=[]
    for i in details:
        data = json.load(open(i.backup_file.path))
        for j in data['smses']['sms']:
            if re.match(r'[a-z A-Z]{2}-[a-z A-Z]{6}', j['@address']):
                d={}
                d['address']=j['@address']
                d['date'] = j['@readable_date']
                list2.append(d)
    return Response(list2)
