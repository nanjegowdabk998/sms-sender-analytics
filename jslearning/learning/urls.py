from django.conf.urls import url
from learning.views import pic_upload, mobiledataupload, readbackupfile

urlpatterns = [
    url(r'^pic_upload',pic_upload ),
    url(r'^mobiledataupload', mobiledataupload),
    url(r'^getjsoncontent', readbackupfile)

]

